const strings = require('../app/strings')

test('returns empty string on emptry string', () => {
  let output = ['']
  
  expect(strings('')).toEqual(output)
})

test('returns the correct combinations', () => {
  let input = 'ab'
  let output = ['', 'a', 'b', 'ab']
  
  expect(strings(input)).toEqual(output)

})
const histogram = require('../app/histogram')

const test_array = [2, 2, 1, 6, 120, 150, 6, 2, 99]

test('Return on Empty Array', () => {
	expect(histogram.grouping([])).toBeUndefined
})

test('Returns Map with grouped output', () => {
	

	expect(histogram.grouping(test_array)).toEqual({'2' : 3, '1' : 1, '6' : 2, '99':1, '99+' : 2})
	
})

test('Has Num & Count Headers', () => {
  let output = histogram.printHistogram(test_array)
  //console.log(output)
  //console.log = jest.fn(histogram.printHistogram)
  expect(output).toMatchObject(/Num | Count/)
})

test('Prints formatted Histogram', () => {
  let output = histogram.printHistogram(test_array)
  let expectedOutput = { 'Num'  : 'Count',
        '1'  :  'x',
        '2'  :  'xxx',
        '6'  :  'xx',
        '99'  :  'x',
    '99+' :  'xx' }
      
  expect(output).toEqual(expectedOutput)
})
const pairs = require('../app/pairs_of_10')
const array = [1,5,4,6,9,5]

test('returns no output on empty array', () => {
  expect(pairs.groups([])).toBeUndefined
})

test('returns sorted array', () => {
  let outputArray = [1,4,5,5,6,9]
  
  expect(pairs.groups(array)).toEqual(outputArray)
  
})

test('returns pairs', () => {
  let outputArray = [[1,9],[4,6],[5,5]]
  
  expect(pairs.pairs(pairs.groups(array))).toEqual(outputArray)
  
})

test('returns pairs in O(n)', () => {
	let output = [[5,5],[4,6],[1,9]]

	expect(pairs.linear_pairs(array)).toEqual(output)
})
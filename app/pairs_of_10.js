//return no output on empty array 
//sort array and add up elements from end to end 
//print only groups that add up to 10

const groups = (array) => {
  if (array.length === 0) { return }
  
  let result = []
  
  result = array.sort() // O(nLogn)
  console.log(result)
  
  return result
  
}

const pairs = (array) => {
  
  let start_index = 0
  let end_index = array.length -1 
  let result = []
  
    
  while (start_index < end_index ) { //O(n)
    
    sum = array[start_index] + array[end_index]
    
    if (sum === 10 ) { //everything O(1)
      result.push([array[start_index], array[end_index]])
      console.log(`(${array[start_index]}, ${array[end_index]})`)
      start_index += 1
      end_index -= 1
    }else if (sum > 10 ) {
      end_index -= 1
    }else if (sum < 10) {
      start_index += 1
    }
     
  }   
  
  return result  // O(1)
}

const linear_pairs = (array) => {
  let hash = {}
  let result = []
  sum = 10

  for (let i = 0; i < array.length; i++)  {
    if (hash[array[i]]) {
      result.push([hash[array[i]], array[i]])
      console.log(hash[array[i]],array[i])
    }else {
      hash[sum - array[i]] = array[i]
    }
  }
  return result
}

module.exports = {
  'groups' : groups,
  'pairs' : pairs,
  'linear_pairs' : linear_pairs
}
// get max number in array 
// group numbers by occurences in dictionary e.g { 3 : 5 }
// group all numbers > 99 under group 99+ 
// print dictionary 

const grouping = (array) => {

	if (array.length == 0) { return } // empty array - O(1)

	let group = {} // O(1)
	const maxNumber = 99
  
  array.forEach((item) => { // O(n)
    
    if (item > maxNumber) { // O(1)
      item = '99+' 
    }
           
    group[`${item}`] =  group[item] + 1 || 1 // O(1)

    
  })
  
  return group

}

const printHistogram = (array) => {
  
  let output = {'Num' : 'Count' }
  console.log('Num | Count')
  
  let mapped = grouping(array) // O(1)
  
  for (const [key, value] of Object.entries(mapped)) { // O(n)
    
    output[key] = `${'x'.repeat(value)}`
    console.log(`${key}` + '   |  ' +`${'x'.repeat(value)}`)
  }
  
  return output
  
} 

module.exports =  {
  'grouping' : grouping,
  'printHistogram' : printHistogram
}
//combination 
// size of combination = 2 ^ size_of_array
// complexity O(n^2)


const strings = (input) => {  
  
  if (input === '') {
    return ['']
  }
  
  let toBeCombined = input.split('')
  let result = []
  let exponent = toBeCombined.length
  let combinations = Math.pow(2,exponent)
  
  //console.log(toBeCombined)
  
  for (let i = 0; i < combinations; i++) {  // O(i)
    let combination = ''
    for (let j = 0; j < toBeCombined.length; j++) { //O(j)
      if ((i & Math.pow(2,j))) {
        combination += toBeCombined[j]
      }
    }
    result.push(combination)
  }
  
  //result = [...result,...toBeCombined]
  console.log(result)
  return result
  
}


module.exports = strings